require './library.rb'

describe Library::Author do

  subject { ::Library::Author.new 1900, 2000, 'Test Testovich' }

  # testing looks a bit weird (checking 2 counters at once) due to class variables side-effects
  context 'Counters' do

    it 'should set both counters to zero initially' do
      expect(described_class.total_comments_quantity).to eq 0
      expect(described_class.comments_quantity).to eq 0
    end

    it 'should increase both counters when adding a comment to an author' do
      subject.add_comment 'Comment #1'
      subject.add_comment 'Comment #2'
      expect(described_class.total_comments_quantity).to eq 2
      expect(described_class.comments_quantity).to eq 2
    end

    it 'should increase both counters when adding comments to a different instance of an author' do
      another_author = ::Library::Author.new 1930, 2005, 'Another Author'
      another_author.add_comment 'Cool author!'
      expect(described_class.total_comments_quantity).to eq 3
      expect(described_class.comments_quantity).to eq 3
    end

    it 'should not increase comments_quantity counter when adding comments to different entities' do
      book = ::Library::PublishedBook.new subject, 'Test Title', 100, 500, ::Time.now
      book.add_comment 'What a wonderful book!'
      book.add_comment "On another thought though, it's just ok"
      expect(described_class.total_comments_quantity).to eq 5
      expect(described_class.comments_quantity).to eq 3
    end

  end

end
