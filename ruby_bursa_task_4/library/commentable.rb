module Library
  module Commentable

    # extend nested module Counters when including this module
    def self.included(base)
      base.extend Counters
    end

    def comments
      @comments ||= ::Array.new
    end

    def add_comment(text)
      self.comments << text
      self.class.increase_counters
      self.comments
    end

    # nested module for class methods
    module Counters

      @@total_comments_counter = 0
      @@comments_quantity      = ::Hash.new

      def total_comments_quantity
        @@total_comments_counter
      end

      def comments_quantity
        @@comments_quantity[self.name.to_s] ||= 0
      end

      def increase_counters
        @@total_comments_counter += 1
        @@comments_quantity[self.name.to_s] ||= 0
        @@comments_quantity[self.name.to_s] += 1
      end

    end

  end
end
